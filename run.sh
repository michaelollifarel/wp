#!/bin/sh

docker build -t wordpress:local . 


# create secret 
kubectl create secret generic mysql-pass --from-literal=password=sqlpass

kubectl apply -f deployment.yaml